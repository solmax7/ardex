package com.howtodoinjava.demo.repository;


import com.howtodoinjava.demo.model.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

    @Query("select p from ProductEntity p where p.name like :name")
    List<ProductEntity> findByProductnameLike(@Param("name")String name);

}
