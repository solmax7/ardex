package com.howtodoinjava.demo.web;


import com.howtodoinjava.demo.exception.RecordNotFoundException;
import com.howtodoinjava.demo.model.OrderEntity;
import com.howtodoinjava.demo.model.OrderItem;
import com.howtodoinjava.demo.model.ProductEntity;
import com.howtodoinjava.demo.model.User;
import com.howtodoinjava.demo.repository.OrderRepository;
import com.howtodoinjava.demo.repository.UserRepository;
import com.howtodoinjava.demo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

@Controller
public class OrderController {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    private CurrentUser currentUser;

    @Autowired
    private OrderService orderService;

    @Autowired
    private ProductsService productsService;

    @Autowired
    BucketCacheService cacheService;

    @GetMapping("/ordersusers")
    // @PreAuthorize("hasRole(ADMIN)")
    public ModelAndView getAllOrdersUsers() {

        User user = currentUser.getUser();

        List<OrderEntity> orderEntities = orderRepository.findAllByUserId(user.getId());

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("orders", orderEntities);
        modelAndView.setViewName("orders.html");
        return modelAndView;
    }

    @GetMapping("/orders")
    public ModelAndView getAllOrders() {

        ModelAndView modelAndView = new ModelAndView();

        List<OrderEntity> orderList = orderService.findAllOrder();

        modelAndView.addObject("orders", orderList);
        modelAndView.setViewName("orders.html");
        return modelAndView;
    }

    @GetMapping("/order/{id}")
    public ModelAndView getOrder(@PathVariable Long id) {

        ModelAndView modelAndView = new ModelAndView();

        Optional<OrderEntity> orderEntity = orderService.getOrder(id);

        modelAndView.addObject("productEntities", orderEntity.get().getProductEntities().stream().collect(toSet()));
        modelAndView.addObject("order", id);
        modelAndView.setViewName("order.html");
        return modelAndView;
    }

    @PostMapping("/addorder")
    public String createOrUpdateProduct(HttpServletRequest request)
            throws RecordNotFoundException {

        OrderEntity orderEntity = new OrderEntity();

        User user = currentUser.getUser();

        List<Long> productsId = cacheService.getUserBucket(String.valueOf(user.getId()));

        List<ProductEntity> products = productsId.stream().map(id -> productsService.getProductById(id))
                .collect(Collectors.toList());

        Map<Long, Long> countByProductId = productsId.stream().collect(Collectors.groupingBy(id -> id, Collectors.counting()));

        orderEntity.setProductEntities(products.stream()
                .map(p -> {
                    OrderItem item = new OrderItem();
                    item.setOrder(orderEntity);
                    item.setProduct(p);
                    item.setSize(Math.toIntExact(countByProductId.get(p.getId())));
                    return item;
                })
                .collect(Collectors.toSet())
        );

        orderEntity.setUser(user);

        orderService.addOrderRepositiry(orderEntity);

        cacheService.saveUserBucket(String.valueOf(user.getId()), new ArrayList<>());
        return "/index";
    }

}
