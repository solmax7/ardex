package com.howtodoinjava.demo.web;


import com.howtodoinjava.demo.exception.RecordNotFoundException;
import com.howtodoinjava.demo.model.ProductEntity;
import com.howtodoinjava.demo.model.User;
import com.howtodoinjava.demo.service.BucketCacheService;
import com.howtodoinjava.demo.service.CurrentUser;
import com.howtodoinjava.demo.service.ProductsService;
import com.howtodoinjava.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

@Controller
public class ProductController {

    public static final String USER_BUCKET_COOKIE = "user_bucket_cookie";
    public static final String DUMMY_PRODUCT_ID = "0";
    private Map<Long, Integer> Basket = new HashMap<>();

    private int count;

    @Autowired
    CurrentUser currentUser;

    @Autowired
    UserService userService;

    @Autowired
    ProductsService service;

    @Autowired
    BucketCacheService cacheService;

    @GetMapping("/searchproduct")

    public ModelAndView getproductlikename(String productname) {
        ModelAndView modelAndView = new ModelAndView();

        List<ProductEntity> list = service.searchProductByName(productname);
        modelAndView.addObject("products", list);
        modelAndView.setViewName("products.html");
        return modelAndView;
    }

    @GetMapping("/productsadmin")

    public ModelAndView getAllProductsAdmin() {

        SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();

        List<ProductEntity> list = service.getAllProducts();
        modelAndView.addObject("products", list);
        modelAndView.setViewName("productsadmin.html");
        return modelAndView;
    }

    @GetMapping("/products")

    public ModelAndView getAllProducts() {

        User user = currentUser.getUser();

        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0].toString();

        SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();

        if (role.equals("ROLE_ADMIN")) {

            modelAndView.setViewName("productsadmin.html");

        } else {
            modelAndView.setViewName("products.html");
        }

        List<ProductEntity> list = service.getAllProducts();
        modelAndView.addObject("products", list);
        return modelAndView;
    }

    @GetMapping("/editproduct/{id}")

    public ModelAndView editProduct(@PathVariable("id") Long id) throws RecordNotFoundException {
        ModelAndView modelAndView = new ModelAndView();

        ProductEntity product = service.getProductById(id);

        modelAndView.addObject("product", product);
        modelAndView.setViewName("add-edit-products.html");
        return modelAndView;
    }

    @GetMapping("/addproduct/")

    public ModelAndView addProduct() throws RecordNotFoundException {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("product", new ProductEntity());
        modelAndView.setViewName("add-edit-products.html");
        return modelAndView;
    }

    @PostMapping("/editproduct")
    public String createOrUpdateProduct(ProductEntity product)
            throws RecordNotFoundException {

        service.createOrUpdateProduct(product);

        return "/index";
    }

    @RequestMapping("/delete/{id}")
    public String deleteProductId(@PathVariable("id") Long id)
            throws RecordNotFoundException {

        service.deleteProductId(id);

        return "/index";
    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.POST)
    public String newOrderPosition(@PathVariable("id") Long id, HttpServletRequest request, HttpServletResponse response) throws RecordNotFoundException {

        Cookie cookie = Optional.ofNullable(request.getCookies())
                .map(Arrays::stream)
                .orElseGet(Stream::empty)
                .filter(c -> Objects.equals(c.getName(), USER_BUCKET_COOKIE))
                .findFirst()
                .filter(v -> !StringUtils.isEmpty(v))
                .orElseGet(() -> new Cookie(USER_BUCKET_COOKIE, ""));


        User user = currentUser.getUser();

        String userString = String.valueOf(user.getId());

        List<Long> values = cacheService.getUserBucket(userString);

        if (values == null) {

            values = new ArrayList<>();

        }

        values.add(id);

        cacheService.saveUserBucket(userString, values);

        response.addCookie(newCookie(
                USER_BUCKET_COOKIE,
                values.stream().map(x -> Long.toString(x)).collect(joining("."))
        ));

        count++;

        return "redirect:/products";

    }

    public static Stream<String> getProductsInBucket(String value) {
        return Arrays.stream(value.contains(".") ? value.split(".") : new String[]{value});
    }

    private Cookie newCookie(String name, String value) {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(30);
        cookie.setPath("/");
        cookie.setDomain("localhost");
        return cookie;
    }
}
