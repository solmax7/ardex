package com.howtodoinjava.demo.web;


import com.howtodoinjava.demo.model.Role;
import com.howtodoinjava.demo.model.User;
import com.howtodoinjava.demo.service.CurrentUser;
import com.howtodoinjava.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    CurrentUser currentUser;

    @PostMapping("/login")
    public String handleLogin() {
        SecurityContextHolder.getContext().getAuthentication();
        return "redirect:/index";
    }

    @GetMapping("/")
    public ModelAndView handleLogin1() {

        User user = currentUser.getUser();

        String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0].toString();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", user.getUsername());
        modelAndView.addObject("role", role);

        if (role.equals("ROLE_ADMIN")) {

            modelAndView.setViewName("admin.html");

        } else {
            modelAndView.setViewName("index.html");
        }
        return modelAndView;

    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(User user, Map<String, Object> model) {

        User userFromDB = userService.findUserByName(user.getUsername());

        if (userFromDB != null) {

            model.put("message", "User exists");
            return "registration";
        }

        user.setActive(true);
        user.setRole(Role.USER);
        userService.saveUser(user);

        return "redirect:/login";
    }
}
