package com.howtodoinjava.demo.service;

import com.howtodoinjava.demo.model.OrderEntity;
import com.howtodoinjava.demo.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;

    public List<OrderEntity> findAllOrder() {

        List<OrderEntity> OrderList = orderRepository.findAll();

        if (OrderList.size() > 0) {
            return OrderList;
        }
        else {
            return new ArrayList<OrderEntity>();
        }
    }

    public Optional<OrderEntity> getOrder(Long id) {

        Optional<OrderEntity> orderEntity = orderRepository.findById(id);

        return  orderEntity;
    }

    public void addOrderRepositiry(OrderEntity orderEntity) {

        orderRepository.save(orderEntity);

    }
}
