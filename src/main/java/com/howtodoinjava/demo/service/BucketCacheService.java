package com.howtodoinjava.demo.service;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class BucketCacheService {
    Cache<String, List<Long>> cache;

    @PostConstruct
    public void init() {
        cache = Caffeine.newBuilder()
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .build();
    }

    public void saveUserBucket(String userId, List<Long> products) {
        cache.put(String.valueOf(userId), products);
    }

    public List<Long> getUserBucket(String userId) {
        return cache.getIfPresent(userId);
    }

    public void putUser(String idUser, List values) {

        cache.put(idUser, values);
    }
}
