package com.howtodoinjava.demo.service;

import com.howtodoinjava.demo.model.User;
import com.howtodoinjava.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User findUserByName(String user){

        User userFromDB = userRepository.findByUsername(user);

        return userFromDB;
    }

    public void saveUser(User user){

        userRepository.save(user);

    }
}


