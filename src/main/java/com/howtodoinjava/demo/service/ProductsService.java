package com.howtodoinjava.demo.service;

import com.howtodoinjava.demo.exception.RecordNotFoundException;
import com.howtodoinjava.demo.model.ProductEntity;
import com.howtodoinjava.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductsService {

    @Autowired
    ProductRepository repository;

    public List<ProductEntity> getAllProducts()

    {
        List<ProductEntity> productList = repository.findAll();

        if (productList.size() > 0) {
            return productList;
        }
         else {
            return new ArrayList<ProductEntity>();
        }
    }

    public ProductEntity getProductById(Long id) throws RecordNotFoundException
    {
        Optional<ProductEntity> product = repository.findById(id);

        if(product.isPresent()) {
            return product.get();
        } else {
            throw new RecordNotFoundException("No employee record exist for given id");
        }
    }

    public ProductEntity createOrUpdateProduct(ProductEntity entity) throws RecordNotFoundException
    {

        if (entity.getId() == null) {
            entity = repository.save(entity);
        }
        Optional<ProductEntity> product = repository.findById(entity.getId());

        if(product.isPresent())
        {
            ProductEntity newEntity = product.get();
            newEntity.setDescription(entity.getDescription());
            newEntity.setPrice(entity.getPrice());
            newEntity.setName(entity.getName());

            newEntity = repository.save(newEntity);

            return newEntity;
        } else {
            entity = repository.save(entity);

            return entity;
        }
    }

    public void deleteProductId(Long id) throws RecordNotFoundException
    {
        Optional<ProductEntity> productEntity = repository.findById(id);

        if(productEntity.isPresent())
        {
            repository.deleteById(id);
        } else {
            throw new RecordNotFoundException("No product record exist for given id");
        }
    }

    public List<ProductEntity> searchProductByName(String productname){

        List<ProductEntity>listproduct =  repository.findByProductnameLike(productname);

        return listproduct;
    }

}



