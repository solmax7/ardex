package com.howtodoinjava.demo.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "ORDERS")
public class OrderEntity extends Model {

    @Column(name = "price")
    private Integer price;

    @ManyToOne
    @JoinColumn(name = "iduser")
    private User user;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "order", cascade = CascadeType.ALL)
    private Set<OrderItem> productEntities;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<OrderItem> getProductEntities() {
        return productEntities;
    }

    public void setProductEntities(Set<OrderItem> productEntities) {
        this.productEntities = productEntities;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

}
